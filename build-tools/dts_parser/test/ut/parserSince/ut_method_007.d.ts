/**
 * the ut for method in sourcefile, method has permission
 * 
 * @permission ohos.permission.GRANT_SENSITIVE_PERMISSIONS
 */
export declare function test(param1: string): Promise<Want>;

/**
 * the ut for method in sourcefile, method has permissions
 * 
 * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS or ohos.permission.GRANT_SENSITIVE_PERMISSIONS or ohos.permission.REVOKE_SENSITIVE_PERMISSIONS
 */
export declare function test(param1: string): Promise<Want>;

/**
 * the ut for method in sourcefile, method has permissions
 * 
 * @permission
 */
export declare function test(param1: string): Promise<Want>;