/**
 * the ut for const in namespace, has the tag of constant, but the type is literal
 */
export namespace test {
  /**
   * @constant
   */
  const name: '2';
}