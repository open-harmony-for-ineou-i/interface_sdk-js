/**
 * the ut for method in class, method is promise
 */
export class Test {
  test(param1: string): Promise<Want>;
}