/**
 * the ut for property, the property is in type alias and is read only
 */
export type test = {
  readonly name: string
}