/**
 * the ut for property in interface, the property has permission,
 * and permission value is null
 */
export interface test {
  /**
   * @permission
   */
  name: string
}