/**
 * the ut for method in sourcefile, method has event subscription
 */
export declare function on(type: 'play', callback: Callback<void>): void;

/**
 * the ut for method in sourcefile, method has event subscription
 */
export declare function off(type: 'play', callback: Callback<void>): void;