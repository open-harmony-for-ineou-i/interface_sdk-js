/**
 * the ut for property in interface, the property has permission,
 * and permission value is not null
 */
export interface test {
  /**
   * @permission ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  name: string
}