export class Test {
  /**
   * the ut for parent node has no JsDoc, some child nodes have two or more JsDoc
   * 
   * @since 6
   */
  /**
   * @since 7
   */
  id: number;
  
  /**
   * @since 7
   */
  /**
   * @since 8
   */
  name: string;

  /**
   * @since 9
   */
  age: number;
}