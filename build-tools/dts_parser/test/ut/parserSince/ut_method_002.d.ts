/**
 * the ut for method in sourcefile, which doesn't have params, but has return value
 */
export declare function test(): number;