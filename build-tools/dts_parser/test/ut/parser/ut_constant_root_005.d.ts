/**
 * the ut for constant with long value
 */
declare const CONSTANT: PropertyDecorator & ((value: string) => PropertyDecorator);
