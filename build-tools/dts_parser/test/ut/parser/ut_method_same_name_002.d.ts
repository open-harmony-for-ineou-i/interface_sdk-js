/**
 * the ut for method in interface
 *
 */
export interface Test {
  (str: string): void;
  (str: string, str2: string): void;
}
