/**
 * the ut for jsdoc about static
 *
 */
export class Test {
  /**
   * @static
   */
  static func(str: string): void;
}
